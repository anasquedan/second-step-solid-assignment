package jo.secondstep.ocp;

public enum EmployeeType {
    MANAGER, ENGINEER
}
